@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">MODULO CLIENTES</div>

                <div class="col text-right">
                    <a href="{{ route('crear.clientes')}}" class="btn btn-sm btn-primary">Nuevo cliente</a>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col"># ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Cedula</th>
                            <th scope="col">Dirección</th>
                            <th scope="col">Teléfono</th>
                            <th scope="col">Fecha de nacimiento</th>
                            <th scope="col">Correo</th>


                            </tr>
                        </thead>
                        <tbody>
                             @foreach ($cliente as $item)                  
                                <tr>
                                    <th scope="row">{{$item->id}}</th>
                                    <td>{{$item->nombre}}</td>
                                    <td>{{$item->apellidos}}</td>
                                    <td>{{$item->cedula}}</td>
                                    <td>{{$item->direccion}}</td>
                                    <td>{{$item->telefono}}</td>
                                    <td>{{$item->fecha_nacimiento}}</td>
                                    <td>{{$item->email}}</td>


                                </tr>
                            @endforeach    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
